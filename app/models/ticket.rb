class Ticket < ActiveRecord::Base
  belongs_to :project
  validates :name, presence: true
  validates :description, presence: true, length: { minimum: 10 }
  #we have run : rails g devise user ; rails g devise:views 
  belongs_to :author , class_name: "User"
  # afterwords rails  g migration add_author_to_tickets author:refereces
end
