class TicketsController < ApplicationController

	before_action :set_project
	before_action :set_ticket, only: [:show, :edit, :update, :destroy]

	def set_project
		#cfr. @project=Project.find(params[:id])
		@project=Project.find(params[:project_id])
	end
	
	#used before in show,edit,update,destroy
	def set_ticket
		@ticket=@project.tickets.find(params[:id])
	rescue ActiveRecord::RecordNotFound
		flash[:alert]="The Ticket you where looking for could not be found"
		redirect_to project_tickets_path
	end

	def ticket_params
		params.require(:ticket).permit(:name, :description)
	end
	
	def new
		#cfr.	@project=Project.new
		#build set the foreign key project_id for the association
		#i.e. project:references in "rails g model ticket ... project:references"
		@ticket=@project.tickets.build
	end

	def create
		#cfr. @project=Project.new(project_params)
		@ticket=@project.tickets.build(ticket_params)
		#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		@ticket.author=current_user
		if @ticket.save
			flash[:notice]="Ticket has been created"
			#Submit data and post request was done.
			#Create a new browser request, 
			#change url for /projects/new to /projects/:id
			#show a single record information
			#cfr. redirect_to @project
			redirect_to [@project, @ticket]
		else
			flash.now[:alert]="Ticket has not been created"
			# render the current url /projects/new ; i.e. : new.html.erb ; /projects/
			render action: :new
		end
	end

	def show
	end
	
	def edit
	end
	
	def update
		if @ticket.update(ticket_params)
			flash[:notice]="Ticket has been updated"
			redirect_to [@project,@ticket]
		else
			flash.now[:alert]="Ticket has not been updated"
			render "edit"
		end
	end
	
	def destroy
		@ticket.destroy
		flash[:notice]="Ticket has been deleted"
		redirect_to @project
	end

	private :set_project, :ticket_params, :set_ticket
	
end
