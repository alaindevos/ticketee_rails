class Admin::ProjectsController < Admin::ApplicationController

	def test
		render "test"
	end

	#GET /projects/new
	def new
		@project=Project.new
	end

	#POST /projects
	def create
		@project=Project.new(project_params)
		if @project.save
			flash[:notice]="Project has been created"
			#Submit data and post request was done.
			#Create a new browser request, 
			#change url for /projects/new to /projects/:id
			#show a single record information
			redirect_to @project
		else
			flash.now[:alert]="Project has not been created"
			#render the current url /projects/new ; i.e. : new.html.erb ; /projects/
			render action: :new
		end
	end
	
	#DELETE /products/1
	def destroy
		set_project
		@project.destroy
		flash[:notice]="Project has been deleted"
		redirect_to projects_path
	end

	private
	
	#used in create
	def project_params
		params.require(:project).permit(:name, :description)
	end

	#used in destroy
	def set_project
		@project=Project.find(params[:id])
	rescue ActiveRecord::RecordNotFound
		flash[:alert]="The project you where looking for could not be found"
		redirect_to projects_path
	end

end
