class ProjectsController < ApplicationController

before_action :set_project, only: [:show, :edit, :update]

#new,create,destroy moved to admin/projects_controller.rb

def test
	render "test"
end

#GET /projects/1
def show
end

#GET /projects
def index
	@projects=Project.all
end

#GET /projects/1/edit
def edit 
end

#PATCH/PUT /projects/1
def update
	if @project.update(project_params)
		flash[:notice]="Project has been updated"
		redirect_to @project
	else
		flash[:alert]="Project has not been updated"
		render action: :edit
	end
end

end

private
def project_params
	params.require(:project).permit(:name, :description)
end

#used before in show,edit,update
def set_project
	@project=Project.find(params[:id])
	rescue ActiveRecord::RecordNotFound
		flash[:alert]="The project you where looking for could not be found"
		redirect_to projects_path
end
