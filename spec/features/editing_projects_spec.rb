require "rails_helper"
RSpec.feature "Users can edit existing projects" do
	before do
		FactoryGirl.create(:project,name:"vim")
		
		visit "/"
		click_link "vim"
		click_link "Edit Project"
	end
	
	scenario "with valid attributes" do
		fill_in "Name", with: "vimnew"
		click_button "submit"
		
		expect(page).to have_content "Project has been updated"
		expect(page).to have_content "vimnew"
	end

	scenario "with invalid attributes" do
		fill_in "Name", with: ""
		click_button "submit"
		
		expect(page).to have_content "Project has not been updated"
	end
	
	
end

		
