require "rails_helper"

RSpec.describe Admin::ApplicationController, type: :controller do
  let(:user) { FactoryGirl.create(:user) }

  before do
		#do nothing for the function authenticate_user in our test.
    allow(controller).to receive(:authenticate_user!)
		#stub current_user method in the controller test, the controller think the user is already signed in.
		#note : you don't do stubbing in feature tests for the whole application.
    allow(controller).to receive(:current_user).and_return(user)
  end

  context "non-admin users" do
    it "are not able to access the index action" do
      get :index

      expect(response).to redirect_to "/"
      expect(flash[:alert]).to eq "You must be an admin"
    end
  end
end
