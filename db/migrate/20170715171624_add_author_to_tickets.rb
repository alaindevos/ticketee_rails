class AddAuthorToTickets < ActiveRecord::Migration
  def change
		#ok there is no tickets table
    add_reference :tickets, :author, index: true
		#foreign key support in mysql
		#bad:we generated a users table , not an authors table
    #add_foreign_key :tickets, :authors
		#good:use users table, but author_id to point to it
    add_foreign_key :tickets, :users, column: :author_id
  end
end
